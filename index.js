const express = require('express')
const bodyParser = require('body-parser')
const studentRoutes = require('./src/routes/student')

const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use('/student', studentRoutes)

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500
  console.error(err.message, err.stack)
  res.status(statusCode).json({ message: err.message })

  return
})

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})
