async function getAll() {}

async function create(student) {}

async function update(id, student) {}

async function remove(id) {}

async function getById(id) {}

module.exports = {
  getAll,
  create,
  update,
  remove,
  getById,
}
